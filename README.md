# eduMedia REST API

Current version: `v1`

## Endpoints

All the endpoints are prefixed with `https://www.edumedia.com/api/v1/{locale}`
or `https://junior.edumedia.com/api/v1/{locale}` depending on
the level you wish to query (`junior` is for primary and `www` is for secondary).

- [Media details](#media-details)
- [Tree item details](#tree-item-details)
- [Search medias](#search-medias)
- [Authorize](#authorize)
- [Validate token](#validate-token)

Valid locales (`locale`) are:
- `en`
- `fr`
- `es`
- `de`

All endpoints return a JSON object.

### Media details

Retrieve a media's details.

**Method:** `GET`

#### Path

```
/media/{id}
```

#### Path parameters

| Name | Value              |
|------|--------------------|
| `id` | Any valid media ID |

#### Query parameters

| Name    | Type   | Value                          |
|---------|--------|--------------------------------|
| `token` | string | Authorization token (optional) |

#### Result properties

| Name           | Type    | Description                                                 | Note                                             |
|----------------|---------|-------------------------------------------------------------|--------------------------------------------------|
| `id`           | int     | ID                                                          ||
| `title`        | string  | Title                                                       ||
| `type`         | string  | Type: `simulation`, `video`, or `doc_pack`                  ||
| `computedType` | string  | Computed type: `simulation`, `video`, `quiz`, or `doc_pack` ||
| `description`  | string  | HTML "Description"                                          ||
| `goals`        | string  | HTML "Learning goals"                                       ||
| `more`         | string  | HTML "Learn more"                                           | Only if provided token is valid                  |
| `tags`         | array   | Array of tags (strings)                                     ||
| `width`        | int     | Width                                                       ||
| `height`       | int     | Height                                                      ||
| `wide`         | boolean | `true` if media is wide                                     ||
| `href`         | string  | URL                                                         | With unlock parameter if provided token is valid |
| `apiURL`       | string  | REST API URL                                                ||
| `frameURL`     | string  | Frame (embed) URL                                           | With unlocked URL if provided token is valid     |
| `thumbnail`    | string  | Thumbnail URL                                               ||

#### Example URL

```
https://www.edumedia.com/api/v1/en/media/167
```

### Tree item details

Retrieve a tree item's details.

**Method:** `GET`

#### Path

```
/tree-item/{api-id}
```

#### Path parameters

| Name     | Value                                       |
|----------|---------------------------------------------|
| `api-id` | `root`, `n-root`, `c-root`, or valid API ID |

##### Notes

eduMedia offers two main entry points from browsing its medias:
- Nodes (`/tree-item/n-root`), where medias are organized by subjects;
- [DEPRECATED] Curriculum (`/tree-item/c-root`), where medias are organized by countries and curriculum.

Those two trees are combined in a parent root (`/tree-item/root`) for easier navigation.

All other tree items are accessible via their API ID (`/tree-item/{api-id}`).

So :
- `/tree-item/root` will list eduMedia's entry points;
- `/tree-item/n-root` will list root nodes;
- [DEPRECATED] `/tree-item/c-root` will list root curriculum;
- `/tree-item/{api-id}` will list any tree item's children.

#### Result properties

| Name       | Type    | Description                                                   |
|------------|---------|---------------------------------------------------------------|
| `id`       | string  | API ID                                                        |
| `title`    | string  | Title                                                         |
| `type`     | string  | Type: `root`, `node`, or `curriculum`                         |
| `leaf`     | boolean | Leaf status                                                   |
| `href`     | string  | URL                                                           |
| `parentID` | string  | Parent tree item API ID (can be `null`)                       |
| `children` | array   | Array of [simplified tree items](#simplified-tree-item)       |
| `medias`   | array   | Array of [simplified media objects](#simplified-media-object) |

#### Example URL

```
https://www.edumedia.com/api/v1/en/tree-item/n-52
```

### Search medias

**Method:** `GET`

#### Path

```
/search
```

#### Query parameters

| Name               | Type   | Value                                                                    |
|--------------------|--------|--------------------------------------------------------------------------|
| `q`                | string | Search query string                                                      |
| `tree-item-api-id` | string | Tree item API ID (optional ;see [Tree item details](#tree-item-details)) |
| `max`              | int    | Maximum results (optional, defaults to `12`)                             |

#### Result properties

| Name     | Type   | Description                                                   |
|----------|--------|---------------------------------------------------------------|
| `href`   | string | Search URL                                                    |
| `q`      | string | Provided query string                                         |
| `medias` | array  | Array of [simplified media objects](#simplified-media-object) |

#### Example URL

```
https://www.edumedia.com/api/v1/en/search?q=monkey
```

### Authorize

Request authorization token.

**Method:** `POST`

#### Path

```
/authorize
```

#### POST data

| Name       | Type   | Value    |
|------------|--------|----------|
| `username` | string | Username |
| `password` | string | Password |

#### Result properties

| Name                         | Type    | Description                  | Note               |
|------------------------------|---------|------------------------------|--------------------|
| `success`                    | boolean | Authorization success        ||
| `clientID`                   | int     | Client ID                    | Only if successful |
| `clientStringRepresentation` | string  | Client string representation | Only if successful |
| `token`                      | string  | Authorization token          | Only if successful |

### Validate token

**Method:** `GET`

```
/validate-token
```

#### Query parameters

| Name    | Type   | Value               |
|---------|--------|---------------------|
| `token` | string | Authorization token |

#### Result properties

| Name      | Type    | Description              |
|-----------|---------|--------------------------|
| `success` | boolean | Token validation success |

## Data types

### Simplified tree item

| Name     | Type    | Description           |
|----------|---------|-----------------------|
| `id`     | string  | API ID                |
| `title`  | string  | Title                 |
| `href`   | string  | URL                   |
| `apiURL` | string  | REST API URL          |
| `leaf`   | boolean | Tree item leaf status |

### Simplified media object

| Name           | Type    | Description                                                 |
|----------------|---------|-------------------------------------------------------------|
| `id`           | int     | ID                                                          |
| `title`        | string  | Title                                                       |
| `type`         | string  | Type: `simulation`, `video`, or `doc_pack`                  |
| `computedType` | string  | Computed type: `simulation`, `video`, `quiz`, or `doc_pack` |
| `width`        | int     | Width                                                       |
| `height`       | int     | Height                                                      |
| `wide`         | boolean | `true` if media is wide                                     |
| `href`         | string  | URL                                                         |
| `apiURL`       | string  | REST API URL                                                |
| `thumbnail`    | string  | Thumbnail URL                                               |